// KHỞI TẠO DANH SÁCH NHÂN VIÊN LẤY TỪ LOCALSTORAGE
var dsnv = [];
var dataJson = localStorage.getItem("DSNV");
if (dataJson != null) {
  dsnv = JSON.parse(dataJson).map(function (item) {
    return new NhanVien(
      item.taiKhoan,
      item.ten,
      item.email,
      item.matKhau,
      item.ngayLam,
      item.luongCoBan,
      item.chucVu,
      item.gioLam
    );
  });
  renderDSNV(dsnv);
}

// HÀM THÊM NHÂN VIÊN (KẾT HỢP KIỂM TRA TÀI KHOẢN VÀ KIỂM TRA THÔNG TIN CHUNG)
function themNhanVien() {
  var nv = layThongTinTuForm();
  var checkValidTaiKhoan =
    kiemTraKhongNhap("tbTKNV", "Không được để trống", nv.taiKhoan) &&
    kiemTraTrungTK(nv.taiKhoan) &&
    kiemTraDoDai(4, 6, "tbTKNV", "Tài khoản từ 4-6 ký tự", nv.taiKhoan);
  var isValid = checkValidTaiKhoan & checkValidThongTinChung(nv);
  if (isValid) {
    dsnv.push(nv);
    renderDSNV(dsnv);
    var dataJson = JSON.stringify(dsnv);
    localStorage.setItem("DSNV", dataJson);
    $("#myModal").modal("hide");
  }
}

// HÀM XÓA NHÂN VIÊN
function xoaNhanVien(id) {
  var index = dsnv.findIndex(function (item) {
    return item.taiKhoan == id;
  });
  dsnv.splice(index, 1);
  renderDSNV(dsnv);
  var dataJson = JSON.stringify(dsnv);
  localStorage.setItem("DSNV", dataJson);
}

// HÀM NHẤN NÚT SỬA NHÂN VIÊN
function suaNhanVien(id) {
  var index = dsnv.findIndex(function (item) {
    return item.taiKhoan == id;
  });
  showThongTinLenForm(dsnv[index]);
  document.getElementById("tknv").disabled = true;
  document.getElementById("btnCapNhat").disabled = false;
  document.getElementById("btnThemNV").disabled = true;
  clearFormSpan();
}

// HÀM CẬP NHẬT NHÂN VIÊN
function capNhatNhanVien() {
  var nv = layThongTinTuForm();
  if (checkValidThongTinChung(nv)) {
    var tk = document.getElementById("tknv").value;
    var index = dsnv.findIndex(function (item) {
      return item.taiKhoan == tk;
    });
    dsnv[index] = nv;
    renderDSNV(dsnv);
    var dataJson = JSON.stringify(dsnv);
    localStorage.setItem("DSNV", dataJson);
    $("#myModal").modal("hide");
  }
}

// HÀM TÌM KIẾM THEO LOẠI (XUẤT SẮC, GIỎI, KHÁ, ...)
function searchLoaiNhanVien(event) {
  var text = event.value;
  text = new RegExp(text, "gi");
  var searchArr = dsnv.filter((nv) => nv.xepLoai().match(text));
  renderDSNV(searchArr);
}
