// RENDER DANH SÁCH NHÂN VIÊN
function renderDSNV(dsnv) {
  var contentHTML = "";
  for (var i = 0; i < dsnv.length; i++) {
    var nv = dsnv[i];
    var content = `<tr>
    <td>${nv.taiKhoan}</td>
    <td>${nv.ten}</td>
    <td>${nv.email}</td>
    <td>${nv.ngayLam}</td>
    <td>${nv.chucVu}</td>
    <td>${nv.tongLuong().toLocaleString()}</td>
    <td>${nv.xepLoai()}</td>
    <td><button onclick="suaNhanVien('${
      nv.taiKhoan
    }')" class="btn btn-success" data-toggle="modal" data-target="#myModal">Sửa</button>
    <button onclick="xoaNhanVien('${
      nv.taiKhoan
    }')" class="btn btn-danger">Xóa</button>
    </td>
    </tr>`;
    contentHTML += content;
  }
  document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

// LẤY THÔNG TIN TỪ FORM
function layThongTinTuForm() {
  var taiKhoan = document.getElementById("tknv").value;
  var ten = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var matKhau = document.getElementById("password").value;
  var ngayLam = document.getElementById("datepicker").value;
  var luongCoBan = document.getElementById("luongCB").value;
  var chucVu = document.getElementById("chucvu").value;
  var gioLam = document.getElementById("gioLam").value;
  return new NhanVien(
    taiKhoan,
    ten,
    email,
    matKhau,
    ngayLam,
    luongCoBan,
    chucVu,
    gioLam
  );
}

// SHOW THÔNG TIN LÊN FORM
function showThongTinLenForm(nv) {
  document.getElementById("tknv").value = nv.taiKhoan;
  document.getElementById("name").value = nv.ten;
  document.getElementById("email").value = nv.email;
  document.getElementById("password").value = nv.matKhau;
  document.getElementById("datepicker").value = nv.ngayLam;
  document.getElementById("luongCB").value = nv.luongCoBan;
  document.getElementById("chucvu").value = nv.chucVu;
  document.getElementById("gioLam").value = nv.gioLam;
}

// KHỞI TẠO LẠI FORM (INPUT VÀ BUTTON)
function clearForm() {
  document.getElementById("formQLNV").reset();
  updateDay();
  clearFormSpan();
  document.getElementById("tknv").disabled = false;
  document.getElementById("btnCapNhat").disabled = true;
  document.getElementById("btnThemNV").disabled = false;
}

// KHỞI TẠO LẠI TẤT CẢ THẺ SPAN TRONG FORM
function clearFormSpan() {
  document.getElementById("tbTKNV").innerText = "";
  document.getElementById("tbTen").innerText = "";
  document.getElementById("tbEmail").innerText = "";
  document.getElementById("tbMatKhau").innerText = "";
  document.getElementById("tbNgay").innerText = "";
  document.getElementById("tbLuongCB").innerText = "";
  document.getElementById("tbChucVu").innerText = "";
  document.getElementById("tbGiolam").innerText = "";
}
