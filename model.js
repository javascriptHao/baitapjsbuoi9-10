// TẠO OBJECT NHÂN VIÊN
function NhanVien(
  taiKhoan,
  ten,
  email,
  matKhau,
  ngayLam,
  luongCoBan,
  chucVu,
  gioLam
) {
  this.taiKhoan = taiKhoan;
  this.ten = ten;
  this.email = email;
  this.matKhau = matKhau;
  this.ngayLam = ngayLam;
  this.luongCoBan = luongCoBan;
  this.chucVu = chucVu;
  this.gioLam = gioLam;
  this.tongLuong = function () {
    switch (this.chucVu) {
      case "Giám đốc":
        return this.luongCoBan * 3;
      case "Trưởng phòng":
        return this.luongCoBan * 2;
      case "Nhân viên":
        return this.luongCoBan * 1;
      default:
        return 0;
    }
  };
  this.xepLoai = function () {
    if (this.gioLam >= 192) {
      return "Xuất sắc";
    } else if (this.gioLam >= 176 && this.gioLam < 192) {
      return "Giỏi";
    } else if (this.gioLam >= 160 && this.gioLam < 176) {
      return "Khá";
    } else {
      return "Trung bình";
    }
  };
}
