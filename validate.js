// HÀM HIỆN THÔNG BÁO CHUNG
function showMessage(idSpan, message) {
  document.getElementById(idSpan).innerText = message;
}

// HÀM KIỂM TRA KHÔNG NHẬP
function kiemTraKhongNhap(idSpan, message, value) {
  var length = value.trim().length;
  if (length != 0) {
    showMessage(idSpan, "");
    return true;
  } else {
    showMessage(idSpan, message);
    return false;
  }
}

// HÀM KIỂM TRA TRÙNG TÀI KHOẢN
function kiemTraTrungTK(id) {
  var index = dsnv.findIndex(function (item) {
    return item.taiKhoan == id;
  });
  if (index == -1) {
    showMessage("tbTKNV", "");
    return true;
  } else {
    showMessage("tbTKNV", "Tài khoản đã tồn tại");
    return false;
  }
}

// HÀM KIỂM TRA ĐỘ DÀI CHUỖI NHẬP VÀO
function kiemTraDoDai(min, max, idSpan, message, value) {
  var length = value.length;
  if (length >= min && length <= max) {
    showMessage(idSpan, "");
    return true;
  } else {
    showMessage(idSpan, message);
    return false;
  }
}

// HÀM KIỂM TRA TÊN PHẢI LÀ CHỮ (KHÔNG ĐƯỢC NHẬP SỐ)
function kiemTraTen(idSpan, message, value) {
  const regex = /^([^0-9]*)$/;
  var check = regex.test(value);
  if (check) {
    showMessage(idSpan, "");
    return true;
  } else {
    showMessage(idSpan, message);
    return false;
  }
}

// HÀM KIỂM TRA EMAIL
function kiemTraEmail(email) {
  const regex =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  var check = regex.test(email);
  if (check) {
    showMessage("tbEmail", "");
    return true;
  } else {
    showMessage("tbEmail", "Email không hợp lệ");
    return false;
  }
}

// HÀM KIỂM TRA MẬT KHẨU
function kiemTraMatKhau(pass) {
  const regex = /^(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&#])[A-Za-z\d@$!%*?&#]*$/;
  var check = regex.test(pass);
  if (check) {
    showMessage("tbMatKhau", "");
    return true;
  } else {
    showMessage(
      "tbMatKhau",
      "Chứa ít nhất 1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt"
    );
    return false;
  }
}

// HÀM KIỂM TRA NGÀY LÀM (MM/DD/YYYY)
function kiemTraNgayLam(date) {
  const regex = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/\d{4}$/;
  var check = regex.test(date);
  if (check) {
    showMessage("tbNgay", "");
    return true;
  } else {
    showMessage("tbNgay", "Ngày không hợp lệ (mm/dd/yyyy)");
    return false;
  }
}

// HÀM KIỂM TRA GIÁ TRỊ SỐ NHẬP VÀO (MIN <= NUMBER <= MAX)
function kiemTraGiaTriSo(min, max, idSpan, message, number) {
  if (number >= min && number <= max) {
    showMessage(idSpan, "");
    return true;
  } else {
    showMessage(idSpan, message);
    return false;
  }
}

// HÀM KIỂM TRA CHỌN CHỨC VỤ
function kiemTraChucVu(value) {
  if (value != "Chọn chức vụ") {
    showMessage("tbChucVu", "");
    return true;
  } else {
    showMessage("tbChucVu", "Vui lòng chọn chức vụ");
    return false;
  }
}

// HÀM KIỂM TRA VALIDATION THÔNG TIN CHUNG (TRỪ KIỂM TRA TÀI KHOẢN)
function checkValidThongTinChung(nv) {
  var isValid =
    kiemTraKhongNhap("tbTen", "Không được để trống", nv.ten) &&
    kiemTraTen("tbTen", "Tên phải là chữ", nv.ten);
  isValid &=
    kiemTraKhongNhap("tbEmail", "Không được để trống", nv.email) &&
    kiemTraEmail(nv.email);
  isValid &=
    kiemTraKhongNhap("tbMatKhau", "Không được để trống", nv.matKhau) &&
    kiemTraDoDai(6, 10, "tbMatKhau", "Mật khẩu từ 6-10 ký tự", nv.matKhau) &&
    kiemTraMatKhau(nv.matKhau);
  isValid &=
    kiemTraKhongNhap("tbNgay", "Không được để trống", nv.ngayLam) &&
    kiemTraNgayLam(nv.ngayLam);
  isValid &=
    kiemTraKhongNhap("tbLuongCB", "Không được để trống", nv.luongCoBan) &&
    kiemTraGiaTriSo(
      1000000,
      20000000,
      "tbLuongCB",
      "Lương cơ bản từ 1 000 000 đến 20 000 000",
      nv.luongCoBan
    );
  isValid &= kiemTraChucVu(nv.chucVu);
  isValid &=
    kiemTraKhongNhap("tbGiolam", "Không được để trống", nv.gioLam) &&
    kiemTraGiaTriSo(
      80,
      200,
      "tbGiolam",
      "Số giờ làm trong tháng từ 80 đến 200 giờ",
      nv.gioLam
    );
  return isValid;
}
